package utilz;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

class LoadSaveTest {

    @Test
    void testGetSpriteAtlas() throws IOException {
        String fileName = "chainsaw.png";
        BufferedImage expectedImage = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/" + fileName)));
        System.out.println("successfully checked for taking data");

        BufferedImage actualImage = LoadSave.GetSpriteAtlas(fileName);

        assertNotNull(actualImage);
        assertImagesEqual(expectedImage, actualImage);
    }

    private void assertImagesEqual(BufferedImage expected, BufferedImage actual) {
        assertEquals(expected.getWidth(), actual.getWidth());
        assertEquals(expected.getHeight(), actual.getHeight());
        assertEquals(expected.getType(), actual.getType());
        for (int y = 0; y < expected.getHeight(); y++) {
            for (int x = 0; x < expected.getWidth(); x++) {
                int expectedRGB = expected.getRGB(x, y);
                int actualRGB = actual.getRGB(x, y);
                assertEquals(expectedRGB, actualRGB);
            }
        }
    }

}
