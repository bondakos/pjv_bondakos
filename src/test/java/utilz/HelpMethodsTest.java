package utilz;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

import main.Game;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


import java.awt.geom.Rectangle2D;

@RunWith(MockitoJUnitRunner.class)
class HelpMethodsTest {
    @Test
    public void testCanMoveHere() {
        float x = 50f;
        float y = 60f;
        float width = 70f;
        float height = 80f;
        int[][] lvlData = new int[10][10];

        boolean result = HelpMethods.CanMoveHere(x, y, width, height, lvlData);

        assertFalse(result);
    }

    @Test
    public void testIsEntityOnFloor_WhenOnFloor_ReturnsTrue() {
        Rectangle2D.Float hitbox = new Rectangle2D.Float(10f, 10f, 20f, 32f);
        int[][] lvlData = new int[10][10];

        boolean result = HelpMethods.IsEntityOnFloor(hitbox, lvlData);

        assertTrue(result);
    }
    @Test
    void testGetEntityYPosUnderRoofOrAboveFloor_WhenFalling_ReturnsYPosUnderRoof() {
        Rectangle2D.Float hitbox = new Rectangle2D.Float(10f, 20f, 32f, 64f);
        float airSpeed = 5f;
        int currentTile = (int) (hitbox.y / Game.TILES_SIZE);
        int tileYPos = currentTile * Game.TILES_SIZE;
        int yOffset = (int) (Game.TILES_SIZE - hitbox.height);
        float expectedYPos = tileYPos + yOffset - 1;

        float result = HelpMethods.GetEntityYPosUnderRoofOrAboveFloor(hitbox, airSpeed);

        assertEquals(expectedYPos, result);
    }
}