package ui;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import gamestates.Gamestate;
import gamestates.Playing;
import main.Game;


public class GameCompletedOverlay {

    private final Playing playing;
    public GameCompletedOverlay(Playing playing) {
        this.playing = playing;
    }


    public void draw(Graphics g) {
        g.setColor(new Color(0, 0, 0, 200));
        g.fillRect(0, 0, Game.GAME_WIDTH, Game.GAME_HEIGHT);

        g.setColor(Color.white);
        Font font = new Font("Arial", Font.BOLD, 48);
        g.setFont(font);

        String gameOverText = "Game Completed";
        int xGameOver = (Game.GAME_WIDTH - g.getFontMetrics().stringWidth(gameOverText)) / 2;
        int yGameOver = 150;
        g.drawString(gameOverText, xGameOver, yGameOver);

        String pressEscText = "Press esc to enter Main Menu!";
        int xPressEsc = (Game.GAME_WIDTH - g.getFontMetrics().stringWidth(pressEscText)) / 2;
        int yPressEsc = 300;
        g.drawString(pressEscText, xPressEsc, yPressEsc);

    }
    public void update() {

    }
    /**
     * Handles the key pressed event.
     *
     * @param e the KeyEvent object
     */
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            playing.resetAll();
            Gamestate.state = Gamestate.MENU;
        }
    }
}
