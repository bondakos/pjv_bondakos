package ui;

import java.awt.*;
import java.awt.event.KeyEvent;

import gamestates.Gamestate;
import gamestates.Playing;
import main.Game;

/**
 * The GameOverOverlay class represents the overlay displayed when the game is over.
 */
public class GameOverOverlay {

	private final Playing playing;


	/**
	 * Constructs a GameOverOverlay object.
	 *
	 * @param playing the Playing game state
	 */
	public GameOverOverlay(Playing playing) {
		this.playing = playing;
	}

	/**
	 * Draws the game over overlay on the screen.
	 *
	 */
	private boolean gameOverLogged = false;

	public void draw(Graphics g) {
		g.setColor(new Color(0, 0, 0, 200));
		g.fillRect(0, 0, Game.GAME_WIDTH, Game.GAME_HEIGHT);

		g.setColor(Color.white);
		Font font = new Font("Arial", Font.BOLD, 48);
		g.setFont(font);

		String gameOverText = "Game Over";
		int xGameOver = (Game.GAME_WIDTH - g.getFontMetrics().stringWidth(gameOverText)) / 2;
		int yGameOver = 150;
		g.drawString(gameOverText, xGameOver, yGameOver);

		String pressEscText = "Press esc to enter Main Menu!";
		int xPressEsc = (Game.GAME_WIDTH - g.getFontMetrics().stringWidth(pressEscText)) / 2;
		int yPressEsc = 300;
		g.drawString(pressEscText, xPressEsc, yPressEsc);


		if (!gameOverLogged) {
			playing.getGame().getLogger().info("The game has been lost.");
			gameOverLogged = true;
		}
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			playing.resetAll();
			Gamestate.state = Gamestate.MENU;
		}
	}
}
