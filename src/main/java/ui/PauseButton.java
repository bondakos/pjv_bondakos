package ui;

import java.awt.Rectangle;

/**
 * The PauseButton class represents a button used for pausing the game.
 */
public class PauseButton {

	protected int x, y, width, height;
	protected Rectangle bounds;

	/**
	 * Constructs a PauseButton object.
	 *
	 * @param x      the x-coordinate of the button's position
	 * @param y      the y-coordinate of the button's position
	 * @param width  the width of the button
	 * @param height the height of the button
	 */
	public PauseButton(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		createBounds();
	}

	private void createBounds() {
		bounds = new Rectangle(x, y, width, height);
	}

	/**
	 * Returns the x-coordinate of the button's position.
	 *
	 * @return the x-coordinate
	 */
	public int getX() {
		return x;
	}

	/**
	 * Sets the x-coordinate of the button's position.
	 *
	 * @param x the x-coordinate
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Returns the y-coordinate of the button's position.
	 *
	 * @return the y-coordinate
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets the y-coordinate of the button's position.
	 *
	 * @param y the y-coordinate
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Returns the width of the button.
	 *
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Sets the width of the button.
	 *
	 * @param width the width
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Returns the height of the button.
	 *
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Sets the height of the button.
	 *
	 * @param height the height
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Returns the bounding rectangle of the button.
	 *
	 * @return the bounding rectangle
	 */
	public Rectangle getBounds() {
		return bounds;
	}

	/**
	 * Sets the bounding rectangle of the button.
	 *
	 * @param bounds the bounding rectangle
	 */
	public void setBounds(Rectangle bounds) {
		this.bounds = bounds;
	}
}
