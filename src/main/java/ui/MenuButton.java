package ui;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import gamestates.Gamestate;
import utilz.LoadSave;
import static utilz.Constants.UI.Buttons.*;

/**
 * The MenuButton class represents a button in the game menu.
 */
public class MenuButton {
	private final int xPos;
    private final int yPos;
    private final int rowIndex;
    private int index;
	private final int xOffsetCenter = B_WIDTH / 2;
	private final Gamestate state;
	private BufferedImage[] imgs;
	private boolean mouseOver, mousePressed;
	private Rectangle bounds;

	/**
	 * Constructs a MenuButton object.
	 *
	 * @param xPos      the x-coordinate of the button's position
	 * @param yPos      the y-coordinate of the button's position
	 * @param rowIndex  the index of the button's row in the sprite atlas
	 * @param state     the Gamestate to apply when the button is clicked
	 */
	public MenuButton(int xPos, int yPos, int rowIndex, Gamestate state) {
		this.xPos = xPos;
		this.yPos = yPos;
		this.rowIndex = rowIndex;
		this.state = state;
		loadImgs();
		initBounds();
	}

	private void initBounds() {
		bounds = new Rectangle(xPos - xOffsetCenter, yPos, B_WIDTH, B_HEIGHT);
	}

	private void loadImgs() {
		imgs = new BufferedImage[3];
		BufferedImage temp = LoadSave.GetSpriteAtlas(LoadSave.MENU_BUTTONS);
		for (int i = 0; i < imgs.length; i++)
			imgs[i] = temp.getSubimage(i * B_WIDTH_DEFAULT, rowIndex * B_HEIGHT_DEFAULT, B_WIDTH_DEFAULT, B_HEIGHT_DEFAULT);
	}

	/**
	 * Draws the menu button on the screen.
	 *
	 * @param g the Graphics object
	 */
	public void draw(Graphics g) {
		g.drawImage(imgs[index], xPos - xOffsetCenter, yPos, B_WIDTH, B_HEIGHT, null);
	}

	/**
	 * Updates the menu button.
	 */
	public void update() {
		index = 0;
		if (mouseOver)
			index = 1;
		if (mousePressed)
			index = 2;
	}

	/**
	 * Sets the mouse over state of the button.
	 *
	 * @param mouseOver the mouse over state
	 */
	public void setMouseOver(boolean mouseOver) {
		this.mouseOver = mouseOver;
	}

	/**
	 * Checks if the button is pressed.
	 *
	 * @return true if the button is pressed, false otherwise
	 */
	public boolean isMousePressed() {
		return mousePressed;
	}

	/**
	 * Sets the mouse pressed state of the button.
	 *
	 * @param mousePressed the mouse pressed state
	 */
	public void setMousePressed(boolean mousePressed) {
		this.mousePressed = mousePressed;
	}

	/**
	 * Returns the bounding rectangle of the button.
	 *
	 * @return the bounding rectangle
	 */
	public Rectangle getBounds() {
		return bounds;
	}

	/**
	 * Applies the associated Gamestate when the button is clicked.
	 */
	public void applyGamestate() {
		Gamestate.state = state;
	}

	/**
	 * Resets the mouse over and mouse pressed states of the button.
	 */
	public void resetBools() {
		mouseOver = false;
		mousePressed = false;
	}
}
