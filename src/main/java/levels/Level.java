//tutorial, but lots of mine
package levels;

import entities.Crabby;
import entities.Skeleton;
import objects.Chainsaw;
import objects.Coin;
import objects.Jumper;
import objects.Spike;
import main.Game;
import utilz.HelpMethods;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import static utilz.HelpMethods.*;

/**
 * The Level class represents a game level.
 */
public class Level {

	private final BufferedImage img;
	private int[][] lvlData;
	private ArrayList<Crabby> crabs;
	private ArrayList<Skeleton> skeletons;
	private ArrayList<Spike> spikes;
	private ArrayList<Chainsaw> chainsaws;
	private ArrayList<Coin> coins;
	private ArrayList<Jumper> jumpers;
	private int maxLvlOffsetX;
	private Point playerSpawn;

	public Level(BufferedImage img) { // do not forget to add here creators
		this.img = img;
		createLevelData();
		createEnemies();
		createObjects();
		calcLvlOffsets();
		calcPlayerSpawn();
	}

	private void calcPlayerSpawn() {
		playerSpawn = GetPlayerSpawn(img);
	}

	private void calcLvlOffsets() {
		int lvlTilesWide = img.getWidth();
		int maxTilesOffset = lvlTilesWide - Game.TILES_IN_WIDTH;
		maxLvlOffsetX = Game.TILES_SIZE * maxTilesOffset;
	}

	private void createEnemies() {
		crabs = GetCrabs(img);
		skeletons = GetSkeletons(img);
	}
	private void createObjects(){
		spikes = HelpMethods.GetSpikes(img);
		coins = HelpMethods.GetCoins(img);
		jumpers = HelpMethods.GetJumpers(img);
		chainsaws = HelpMethods.GetChainsaws(img);
	}

	private void createLevelData() {
		lvlData = GetLevelData(img);
	}

	public int getSpriteIndex(int x, int y) {
		return lvlData[y][x];
	}

	public int[][] getLevelData() {
		return lvlData;
	}

	public int getLvlOffset() {
		return maxLvlOffsetX;
	}

	public ArrayList<Crabby> getCrabs() {
		return crabs;
	}
	public ArrayList<Skeleton> getSkeletons(){
		return skeletons;
	}
	public ArrayList<Spike> getSpikes() {
		return spikes;
	}
	public ArrayList<Chainsaw> getChainsaws() {
		return chainsaws;
	}
	public ArrayList<Coin> getCoins() {
		return coins;
	}
	public ArrayList<Jumper> getJumpers() {
		return jumpers;
	}

	public Point getPlayerSpawn() {
		return playerSpawn;
	}
}
