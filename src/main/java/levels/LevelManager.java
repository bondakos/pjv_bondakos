//tutorial, but changed and optimalized
package levels;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.logging.Logger;

import entities.Enemy;
import gamestates.Gamestate;
import main.Game;
import utilz.LoadSave;

/**
 * The LevelManager class manages the levels in the game.
 */
public class LevelManager {
	private static final Logger logger = Logger.getLogger(Enemy.class.getName());
	private  Game game;
	private BufferedImage[] levelSprite;
	private  ArrayList<Level> levels;
	private int lvlIndex = 0;

	/**
	 * Constructs a new LevelManager object with the specified game.
	 *
	 * @param game the game instance
	 */
	public LevelManager(Game game) {
		this.game = game;
		importOutsideSprites();
		levels = new ArrayList<>();
		buildAllLevels();
	}

	/**
	 * Loads the next level in the game.
	 */
	public void loadNextLevel() {
		lvlIndex++;
		logger.log(java.util.logging.Level.INFO, "You successfully passed the level");
		if (lvlIndex >= levels.size()) {
			lvlIndex = 0;
			Gamestate.state = Gamestate.MENU;
			logger.log(java.util.logging.Level.INFO, "You successfully passed the game");
		}

		Level newLevel = levels.get(lvlIndex);
		game.getPlaying().getPlayer().getHealpotion().setAmount(game.getPlaying().getPlayer().getHealpotion().getAmount() + 2);
		game.getPlaying().getPlayer().getEnergypotion().setAmount(game.getPlaying().getPlayer().getEnergypotion().getAmount() + 1);
		game.getPlaying().getPlayer().getShield().setAmount(game.getPlaying().getPlayer().getShield().getAmount());
		game.getPlaying().getPlayer().getCoin().setAmount(game.getPlaying().getPlayer().getCoin().getAmount());
		game.getPlaying().getEnemyManager().loadEnemies(newLevel);
		game.getPlaying().getPlayer().loadLvlData(newLevel.getLevelData());
		game.getPlaying().setMaxLvlOffset(newLevel.getLvlOffset());
		game.getPlaying().getObjectManager().loadObjects(newLevel);
	}

	private void buildAllLevels() {
		BufferedImage[] allLevels = LoadSave.GetAllLevels();
		for (BufferedImage img : allLevels)
			levels.add(new Level(img));
	}

	private void importOutsideSprites() {
		BufferedImage img = LoadSave.GetSpriteAtlas(LoadSave.LEVEL_ATLAS);
		levelSprite = new BufferedImage[48];
		for (int j = 0; j < 4; j++)
			for (int i = 0; i < 12; i++) {
				int index = j * 12 + i;
				levelSprite[index] = img.getSubimage(i * 32, j * 32, 32, 32);
			}
	}

	/**
	 * Draws the current level on the graphics context with the specified level offset.
	 *
	 * @param g          the Graphics object to draw on
	 * @param lvlOffset  the level offset in pixels
	 */
	public void draw(Graphics g, int lvlOffset) {
		for (int j = 0; j < Game.TILES_IN_HEIGHT; j++)
			for (int i = 0; i < levels.get(lvlIndex).getLevelData()[0].length; i++) {
				int index = levels.get(lvlIndex).getSpriteIndex(i, j);
				g.drawImage(levelSprite[index], Game.TILES_SIZE * i - lvlOffset, Game.TILES_SIZE * j, Game.TILES_SIZE, Game.TILES_SIZE, null);
			}
	}

	/**
	 * Updates the level manager.
	 */
	public void update() {

	}
	public void setLevelIndex(int lvlIndex) {
		this.lvlIndex = lvlIndex;
	}
	/**
	 * Returns the current level.
	 *
	 * @return the current level
	 */
	public Level getCurrentLevel() {
		return levels.get(lvlIndex);
	}
	public int getLevelIndex() {
		return lvlIndex;
	}
	public int getAmountOfLevels() {
		return levels.size();
	}

}
