//tutorial, but changed
package entities;

import static utilz.Constants.EnemyConstants.*;
import static utilz.HelpMethods.*;

import java.awt.geom.Rectangle2D;

import static utilz.Constants.Directions.*;
import static utilz.Constants.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import main.Game;

/**
 * The abstract Enemy class represents an enemy entity in the game.
 * It provides common functionality and attributes for different types of enemies.
 */
public abstract class Enemy extends Entity {

	protected int enemyType;
	protected boolean firstUpdate = true;
	protected int walkDir = LEFT;
	protected int tileY;
	protected float attackDistance = Game.TILES_SIZE;
	protected boolean active = true;
	protected boolean attackChecked;
	protected boolean canHurt = true;
	private static final Logger logger = Logger.getLogger(Enemy.class.getName());

	/**
	 * Constructs a new instance of the Enemy class.
	 *
	 * @param x         The x-coordinate of the enemy's position.
	 * @param y         The y-coordinate of the enemy's position.
	 * @param width     The width of the enemy.
	 * @param height    The height of the enemy.
	 * @param enemyType The type of the enemy.
	 */
	public Enemy(float x, float y, int width, int height, int enemyType) {
		super(x, y, width, height);
		this.enemyType = enemyType;

		maxHealth = GetMaxHealth(enemyType);
		currentHealth = maxHealth;
		walkSpeed = Game.SCALE * 0.35f;
	}

	// ...

	/**
	 * Checks if it's the first update and the entity is in the air.
	 *
	 * @param lvlData The level data representing the game environment.
	 */
	protected void firstUpdateCheck(int[][] lvlData) {
		if (!IsEntityOnFloor(hitbox, lvlData))
			inAir = true;
		firstUpdate = false;
	}

	/**
	 * Updates the position of the enemy in the air.
	 *
	 * @param lvlData The level data representing the game environment.
	 */
	protected void updateInAir(int[][] lvlData) {
		if (CanMoveHere(hitbox.x, hitbox.y + airSpeed, hitbox.width, hitbox.height, lvlData)) {
			hitbox.y += airSpeed;
			airSpeed += GRAVITY;
		} else {
			inAir = false;
			hitbox.y = GetEntityYPosUnderRoofOrAboveFloor(hitbox, airSpeed);
			tileY = (int) (hitbox.y / Game.TILES_SIZE);
		}
	}

	/**
	 * Moves the enemy horizontally.
	 *
	 * @param lvlData The level data representing the game environment.
	 */
	protected void move(int[][] lvlData) {
		float xSpeed;

		if (walkDir == LEFT)
			xSpeed = -walkSpeed;
		else
			xSpeed = walkSpeed;

		if (CanMoveHere(hitbox.x + xSpeed, hitbox.y, hitbox.width, hitbox.height, lvlData))
			if (IsFloor(hitbox, xSpeed, lvlData)) {
				hitbox.x += xSpeed;
				return;
			}

		changeWalkDir();
	}

	/**
	 * Turns the enemy towards the player's direction.
	 *
	 * @param player The player entity in the game.
	 */
	protected void turnTowardsPlayer(Player player) {
		if (player.hitbox.x > hitbox.x)
			walkDir = RIGHT;
		else
			walkDir = LEFT;
	}

	/**
	 * Checks if the player is visible to the enemy.
	 *
	 * @param lvlData The level data representing the game environment.
	 * @param player  The player entity in the game.
	 * @return true if the player is visible to the enemy, false otherwise.
	 */
	protected boolean canSeePlayer(int[][] lvlData, Player player) {
		int playerTileY = (int) (player.getHitbox().y / Game.TILES_SIZE);
		if (playerTileY == tileY)
			if (isPlayerInRange(player)) {
                return IsSightClear(lvlData, hitbox, player.hitbox, tileY);
			}
//		logger.log(Level.INFO, "Crabby saw you");
		/*I comment it because of spam in LOG console. Because it checks every gametick. so it can be
		checked, but You will have so many logs about it*/
		return false;
	}

	/**
	 * Checks if the player is within the attack range.
	 *
	 * @param player The player entity in the game.
	 * @return true if the player is within the attack range, false otherwise.
	 */
	protected boolean isPlayerInRange(Player player) {
		int absValue = (int) Math.abs(player.hitbox.x - hitbox.x);
		return absValue <= attackDistance * 5;
	}

	/**
	 * Checks if the player is close enough for the enemy to attack.
	 *
	 * @param player The player entity in the game.
	 * @return true if the player is close enough for the enemy to attack, false otherwise.
	 */
	protected boolean isPlayerCloseForAttack(Player player) {
		int absValue = (int) Math.abs(player.hitbox.x - hitbox.x);
		return absValue <= attackDistance;
	}

	/**
	 * Sets a new state for the enemy.
	 *
	 * @param enemyState The new state of the enemy.
	 */
	protected void newState(int enemyState) {
		this.state = enemyState;
		aniTick = 0;
		aniIndex = 0;
	}

	/**
	 * Reduces the enemy's health.
	 *
	 * @param amount The amount of health to reduce.
	 */
	public void hurt(int amount) {
		if (canHurt()){
			currentHealth -= amount;
			if (currentHealth <= 0) {
				newState(DEAD);
				switch (enemyType) {
					case CRABBY -> logger.log(Level.INFO, "Successfully -1 crabby");
					case SKELETON -> logger.log(Level.INFO, "Successfully -1 skeleton");
				}
			}
			else
				newState(HIT);
		}

	}

	private boolean canHurt() {
		return canHurt;
	}

	/**
	 * Checks if the player is hit by the enemy's attack.
	 *
	 * @param attackBox The attack box of the enemy.
	 * @param player    The player entity in the game.
	 */
	protected void checkPlayerHit(Rectangle2D.Float attackBox, Player player) {
		if (attackBox.intersects(player.hitbox) && !player.isInvulnerable()) {
			player.changeHealth(-GetEnemyDmg(enemyType));
		}
		switch(enemyType) {
			case CRABBY ->
				logger.log(Level.INFO, "Crabby attacked you");
			case SKELETON ->
				logger.log(Level.INFO, "Skeleton attacked you");
		}
		attackChecked = true;
	}

	/**
	 * Updates the animation tick for sprite animation.
	 */
	protected void updateAnimationTick() {
		aniTick++;
		if (aniTick >= ANI_SPEED) {
			aniTick = 0;
			aniIndex++;
			if (aniIndex >= GetSpriteAmount(enemyType, state)) {
				aniIndex = 0;
				switch (state) {
					case ATTACK, HIT -> state = IDLE;
					case DEAD -> active = false;
				}
			}
		}
	}

	/**
	 * Changes the walk direction of the enemy.
	 */
	protected void changeWalkDir() {
		if (walkDir == LEFT)
			walkDir = RIGHT;
		else
			walkDir = LEFT;
	}

	/**
	 * Resets the enemy to its initial state.
	 */
	public void resetEnemy() {
		hitbox.x = x;
		hitbox.y = y;
		firstUpdate = true;
		currentHealth = maxHealth;
		newState(IDLE);
		active = true;
		airSpeed = 0;
	}

	/**
	 * Checks if the enemy is active.
	 *
	 * @return true if the enemy is active, false otherwise.
	 */
	public boolean isActive() {
		return active;
	}
}
