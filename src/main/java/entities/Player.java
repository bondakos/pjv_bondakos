//tutorial, but changed.
// i debugged healthbar, added energy, haste and potions, shield and coin.
// and i debugged some logic here
package entities;

import static utilz.Constants.PlayerConstants.*;
import static utilz.HelpMethods.*;
import static utilz.Constants.*;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import gamestates.Playing;

import main.Game;
import utilz.LoadSave;

import inventory.*;


/**
 * The Player class represents the player entity in the game.
 */
public class Player extends Entity {
	private static final Logger logger = Logger.getLogger(Enemy.class.getName());
	private final HealPotion healpotion = new HealPotion(3);
	private final EnergyPotion energypotion = new EnergyPotion(2);
	private final Shield shield = new Shield(1);
	private final Coin coin = new Coin(0);
	private BufferedImage[][] animations;
	private boolean moving = false, attacking = false;
	private boolean left, right, jump;
	private int[][] lvlData;

	// Jumping / Gravity
	private final float jumpSpeed = -2.3f * Game.SCALE;

	// StatusBarUI
	private BufferedImage statusBarImg;

	//bar vars
	private final int healthBarWidth = (int) (150 * Game.SCALE);
	private final int energyBarWidth = (int) (104 * Game.SCALE);
	private int healthWidth = healthBarWidth;
	private int energyWidth = energyBarWidth;

	private int flipX = 0;
	private int flipW = 1;

	private boolean attackChecked;
	private final Playing playing;

	private final int maxEnergy = 40;
	public int currentEnergy;
	private int energyRecoveryTimer = 0;
	private boolean invulnerable = false;

	/**
	 * Constructs a new Player object with the specified coordinates, dimensions, and playing state.
	 *
	 * @param x       The x-coordinate of the player.
	 * @param y       The y-coordinate of the player.
	 * @param width   The width of the player.
	 * @param height  The height of the player.
	 * @param playing The playing state associated with the player.
	 */
	public Player(float x, float y, int width, int height, Playing playing) {
		super(x, y, width, height);
		this.playing = playing;
		this.state = IDLE;
		this.maxHealth = 100;
		this.currentHealth = maxHealth;
		this.currentEnergy = maxEnergy;
		this.walkSpeed = Game.SCALE;
		loadAnimations();
		initHitbox(20, 27);
		initAttackBox();
	}

	/**
	 * Sets the spawn point of the player to the specified coordinates.
	 *
	 * @param spawn The spawn point coordinates.
	 */
	public void setSpawn(Point spawn) {
		this.x = spawn.x;
		this.y = spawn.y;
		hitbox.x = x;
		hitbox.y = y;
	}

	private void initAttackBox() {
		attackBox = new Rectangle2D.Float(x, y, (int) (20 * Game.SCALE), (int) (20 * Game.SCALE));
	}

	/**
	 * Updates the player's state and position.
	 */
	public void update() {
		updateHealthBar();
		updateEnergyBar();

		if (currentHealth <= 0) {
			playing.setGameOver(true);
			return;
		}

		updateAttackBox();

		updatePos();
		if (moving) {
			checkSpikesTouched();
			checkCoinTouched();
			checkJumperTouched();
		}
		if (attacking)
			checkAttack();
		checkChainsawTouched();
		updateAnimationTick();
		setAnimation();
	}
	private void checkSpikesTouched() {
		playing.checkSpikesTouched(this);
	}
	private void checkChainsawTouched() {
		playing.checkChainsawTouchedPlayer(this);
	}
	private void checkCoinTouched() {
		playing.checkCoinTouchedPlayer(this);
	}
	private void checkJumperTouched() {
		playing.checkJumperTouched(this);
	}
	private void checkAttack() {
		if (attackChecked || aniIndex != 1)
			return;
		attackChecked = true;
		playing.checkEnemyHit(attackBox);

	}

	private void updateAttackBox() {
		if (right)
			attackBox.x = hitbox.x + hitbox.width + (int) (Game.SCALE * 10);
		else if (left)
			attackBox.x = hitbox.x - hitbox.width - (int) (Game.SCALE * 10);

		attackBox.y = hitbox.y + (Game.SCALE * 10);
	}

	private void updateHealthBar() {

		healthWidth = (int) ((currentHealth / (float) maxHealth) * healthBarWidth);
	}
	private void updateEnergyBar() {
		energyWidth = (int) ((currentEnergy / (float) maxEnergy) * energyBarWidth);
		energyRecoveryTimer++;
		if (energyRecoveryTimer >= 600) {
			energyRecoveryTimer = 0;
			if (currentEnergy < maxEnergy) {
				int energyRecoveryRate = 5;
				currentEnergy += energyRecoveryRate;
				if (currentEnergy > maxEnergy) {
					currentEnergy = maxEnergy;
				}
			}
		}
	}
	public void haste(int distance) {
		if (currentEnergy >= 20) {
			float newX;
			if (right) {
				newX = hitbox.x + distance;
			} else if (left) {
				newX = hitbox.x - distance;
			} else {
				return;
			}
			if (CanMoveHere(newX, hitbox.y, hitbox.width, hitbox.height, lvlData)) {
				hitbox.x = newX;
				currentEnergy -= 20;
			}
		}
	}

	/**
	 * Renders the player entity and the UI elements.
	 *
	 * @param g          The Graphics object for rendering.
	 * @param lvlOffset  The level offset.
	 */
	public void render(Graphics g, int lvlOffset) {
//		drawHitbox(g, lvlOffset);
		float xDrawOffset = 21 * Game.SCALE;
		float yDrawOffset = 4 * Game.SCALE;
		g.drawImage(animations[state][aniIndex], (int) (hitbox.x - xDrawOffset) - lvlOffset + flipX,
				(int) (hitbox.y - yDrawOffset), width * flipW, height, null);
		drawShield(g, lvlOffset);
		drawUI(g);
	}
	public void drawShield(Graphics g, int xLvlOffset) {
		if (isInvulnerable()) {
			Graphics2D g2d = (Graphics2D) g;
			g2d.setColor(Color.BLUE);

			int shieldThickness = 5;
			g2d.setStroke(new BasicStroke(shieldThickness));

			int shieldRadius = (int) (Game.SCALE * 18);
			int shieldDiameter = shieldRadius * 2;
			g2d.drawOval((int) hitbox.x - xLvlOffset - (int) Game.SCALE * 12,
					(int) hitbox.y - (int) Game.SCALE * 5,
					shieldDiameter, shieldDiameter);
		}
	}
	private void drawUI(Graphics g) {
		int statusBarWidth = (int) (192 * Game.SCALE);
		int statusBarHeight = (int) (58 * Game.SCALE);
		int statusBarX = (int) (10 * Game.SCALE);
		int statusBarY = (int) (10 * Game.SCALE);
		g.drawImage(statusBarImg, statusBarX, statusBarY, statusBarWidth, statusBarHeight, null);

		g.setColor(Color.red);
		int healthBarHeight = (int) (4 * Game.SCALE);
		int healthBarXStart = (int) (34 * Game.SCALE);
		int healthBarYStart = (int) (14 * Game.SCALE);
		g.fillRect(healthBarXStart + statusBarX, healthBarYStart + statusBarY, healthWidth, healthBarHeight);

		g.setColor(Color.blue);
		int energyBarHeight = (int) (2 * Game.SCALE);
		int energyBarXStart = (int) (44 * Game.SCALE);
		int energyBarYStart = (int) (34 * Game.SCALE);
		g.fillRect(energyBarXStart + statusBarX, energyBarYStart + statusBarY, energyWidth, energyBarHeight);

		healpotion.drawPotion(g, healpotion.getAmount(), statusBarWidth +10, statusBarY, 50, 50);
		energypotion.drawPotion(g, energypotion.getAmount(), statusBarWidth -50, statusBarY +50, 50, 50);
		shield.drawShield(g, shield.getAmount(), 85, statusBarY +75, 50, 50);
		coin.drawCoin(g, coin.getAmount(), 170, statusBarY +75, 50, 50);
	}

	private void updateAnimationTick() {
		aniTick++;
		if (aniTick >= ANI_SPEED) {
			aniTick = 0;
			aniIndex++;
			if (aniIndex >= GetSpriteAmount(state)) {
				aniIndex = 0;
				attacking = false;
				attackChecked = false;
			}
		}
	}

	private void setAnimation() {
		int startAni = state;

		if (moving)
			state = RUNNING;
		else
			state = IDLE;

		if (inAir) {
			if (airSpeed < 0)
				state = JUMP;
			else
				state = FALLING;
		}

		if (attacking) {
			state = ATTACK;
			if (startAni != ATTACK) {
				aniIndex = 1;
				aniTick = 0;
				return;
			}
		}
		if (startAni != state)
			resetAniTick();

	}

	private void resetAniTick() {
		aniTick = 0;
		aniIndex = 0;
	}

	private void updatePos() {
		moving = false;

		if (jump)
			jump();

		if (!inAir)
			if ((!left && !right) || (right && left))
				return;

		float xSpeed = 0;

		if (left) {
			xSpeed -= walkSpeed;
			flipX = width;
			flipW = -1;
		}
		if (right) {
			xSpeed += walkSpeed;
			flipX = 0;
			flipW = 1;
		}

		if (!inAir)
			if (!IsEntityOnFloor(hitbox, lvlData))
				inAir = true;

		if (inAir) {
			if (CanMoveHere(hitbox.x, hitbox.y + airSpeed, hitbox.width, hitbox.height, lvlData)) {
				hitbox.y += airSpeed;
				airSpeed += GRAVITY;
				updateXPos(xSpeed);
			} else {
				hitbox.y = GetEntityYPosUnderRoofOrAboveFloor(hitbox, airSpeed);
				float fallSpeedAfterCollision = 0.5f * Game.SCALE;
				if (airSpeed > 0)
					resetInAir();
				else
					airSpeed = fallSpeedAfterCollision;
				updateXPos(xSpeed);
			}

		} else
			updateXPos(xSpeed);
		moving = true;
	}
	public void kill() {
		currentHealth = 0;
	}
	public void addCoin() {
		coin.setAmount(coin.getAmount() + 1);
	}
	private void jump() {
		if (inAir)
			return;
		inAir = true;
		airSpeed = jumpSpeed;
	}
	public void jumper(float distance){
		if (inAir)
			return;
		inAir = true;
		airSpeed = jumpSpeed*distance;
	}
	private void resetInAir() {
		inAir = false;
		airSpeed = 0;
	}

	private void updateXPos(float xSpeed) {
		if (CanMoveHere(hitbox.x + xSpeed, hitbox.y, hitbox.width, hitbox.height, lvlData))
			hitbox.x += xSpeed;
		else
			hitbox.x = GetEntityXPosNextToWall(hitbox, xSpeed);
	}

	/**
	 * Changes the player's health by the given value.
	 *
	 * @param value  The value to change the health by.
	 */
	public void changeHealth(float value) {
        currentHealth = (int) (currentHealth + value);

		if (currentHealth <= 0)
			currentHealth = 0;
		else if (currentHealth >= maxHealth)
			currentHealth = maxHealth;
	}

	/**
	 * Adds health to the player using a potion.
	 *
	 * @param value  The value to increase the health by.
	 */
	public void addHealth(int value) {
		if (healpotion.getAmount() > 0) {
			healpotion.setAmount(healpotion.getAmount() - 1);
			currentHealth += value;
			logger.log(Level.INFO, "You successfully used the potion");

			if (currentHealth <= 0)
				currentHealth = 0;
			else if (currentHealth >= maxHealth)
				currentHealth = maxHealth;
		}

	}
	public void addEnergy(int value) {
		if (energypotion.getAmount() > 0) {
			energypotion.setAmount(energypotion.getAmount() - 1);
			currentEnergy += value;
			logger.log(Level.INFO, "You successfully used the energy potion");
			if (currentEnergy <= 0)
				currentEnergy = 0;
			else if (currentEnergy >= maxEnergy)
				currentEnergy = maxEnergy;
		}
	}

	public void addShield(boolean value) {
		if (shield.getAmount() > 0) {
			shield.setAmount(shield.getAmount() - 1);
			if (value) {
				setInvulnerable(true);
				logger.log(Level.INFO, "You successfully used the Shield");
				Timer shieldTimer = new Timer();
				shieldTimer.schedule(new TimerTask() {
					@Override
					public void run() {
						setInvulnerable(false);
					}
				}, 5000);
			}
		}
	}
	public void setInvulnerable(boolean invulnerable) {
		this.invulnerable = invulnerable;
	}

	public boolean isInvulnerable() {
		return invulnerable;
	}

	/**
	 * Loads the player's animations from the sprite atlas.
	 */
	private void loadAnimations() {
		BufferedImage img = LoadSave.GetSpriteAtlas(LoadSave.PLAYER_ATLAS);
		animations = new BufferedImage[7][8];
		for (int j = 0; j < animations.length; j++)
			for (int i = 0; i < animations[j].length; i++)
				animations[j][i] = img.getSubimage(i * 64, j * 40, 64, 40);

		statusBarImg = LoadSave.GetSpriteAtlas(LoadSave.STATUS_BAR);
	}

	/**
	 * Loads the level data for collision detection.
	 *
	 * @param lvlData  The level data array.
	 */
	public void loadLvlData(int[][] lvlData) {
		this.lvlData = lvlData;
		if (!IsEntityOnFloor(hitbox, lvlData))
			inAir = true;
	}

	/**
	 * Resets the direction booleans and in-air state of the player.
	 */
	public void resetDirBooleans() {
		left = false;
		right = false;
	}

	/**
	 * Sets the attacking state of the player.
	 *
	 * @param attacking  The attacking state to set.
	 */
	public void setAttacking(boolean attacking) {
		this.attacking = attacking;
	}

	/**
	 * Checks if the player is moving to the left.
	 *
	 * @return true if the player is moving left, false otherwise.
	 */
	public boolean isLeft() {
		return left;
	}

	/**
	 * Sets the moving left state of the player.
	 *
	 * @param left  The moving left state to set.
	 */
	public void setLeft(boolean left) {
		this.left = left;
	}

	/**
	 * Checks if the player is moving to the right.
	 *
	 * @return true if the player is moving right, false otherwise.
	 */
	public boolean isRight() {
		return right;
	}

	/**
	 * Sets the moving right state of the player.
	 *
	 * @param right  The moving right state to set.
	 */
	public void setRight(boolean right) {
		this.right = right;
	}


	/**
	 * Sets the jumping state of the player.
	 *
	 * @param jump  The jumping state to set.
	 */
	public void setJump(boolean jump) {
		this.jump = jump;
	}
	public HealPotion getHealpotion() {
		return healpotion;
	}
	public EnergyPotion getEnergypotion(){
		return energypotion;
	}
	public Shield getShield(){
		return shield;
	}
	public Coin getCoin(){
		return coin;
	}
	public void resetAll() {
		resetDirBooleans();
		inAir = false;
		attacking = false;
		moving = false;
		state = IDLE;
		currentHealth = maxHealth;

		hitbox.x = x;
		hitbox.y = y;

		if (!IsEntityOnFloor(hitbox, lvlData))
			inAir = true;
	}
}
