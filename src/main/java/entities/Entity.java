//tutorial, but changed
package entities;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

import main.Game;

public abstract class Entity {

	protected float x, y;
	protected int width, height;
	protected Rectangle2D.Float hitbox;
	protected int aniTick, aniIndex;
	protected int state;
	protected float airSpeed;
	protected boolean inAir = false;
	protected int maxHealth;
	protected int currentHealth;
	protected Rectangle2D.Float attackBox;
	protected float walkSpeed;

	/**
	 * Constructs an Entity object.
	 *
	 * @param x      The x-coordinate of the entity.
	 * @param y      The y-coordinate of the entity.
	 * @param width  The width of the entity.
	 * @param height The height of the entity.
	 */
	public Entity(float x, float y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	/**
	 * Draws the attack box of the entity.
	 *
	 * @param g          The Graphics object for drawing.
	 * @param xLvlOffset The x offset of the level.
	 */
	//it must be
	protected void drawAttackBox(Graphics g, int xLvlOffset) {
		g.setColor(Color.BLACK);
		g.drawRect((int) (attackBox.x - xLvlOffset), (int) attackBox.y, (int) attackBox.width, (int) attackBox.height);
	}

	/**
	 * Draws the hitbox of the entity.
	 *
	 * @param g          The Graphics object for drawing.
	 * @param xLvlOffset The x offset of the level.
	 */
	//it must be
	protected void drawHitbox(Graphics g, int xLvlOffset) {
		// For debugging the hitbox
		g.setColor(Color.RED);
		g.drawRect((int) hitbox.x - xLvlOffset, (int) hitbox.y, (int) hitbox.width, (int) hitbox.height);
	}
	protected void drawHealthbar(Graphics g, int xLvlOffset){
		g.setColor(Color.RED);
		g.fillRect((int) hitbox.x - xLvlOffset, (int) (hitbox.y - Game.SCALE*10),
				(int) hitbox.width * currentHealth/maxHealth,
				(int) hitbox.height/4);
	}
	/**
	 * Initializes the hitbox of the entity.
	 *
	 * @param width  The width of the hitbox.
	 * @param height The height of the hitbox.
	 */
	protected void initHitbox(int width, int height) {
		hitbox = new Rectangle2D.Float(x, y, (int) (width * Game.SCALE), (int) (height * Game.SCALE));
	}

	/**
	 * Returns the hitbox of the entity.
	 *
	 * @return The hitbox of the entity.
	 */
	public Rectangle2D.Float getHitbox() {
		return hitbox;
	}

	/**
	 * Returns the state of the entity.
	 *
	 * @return The state of the entity.
	 */
	public int getState() {
		return state;
	}

	/**
	 * Returns the animation index of the entity.
	 *
	 * @return The animation index of the entity.
	 */
	public int getAniIndex() {
		return aniIndex;
	}
}
