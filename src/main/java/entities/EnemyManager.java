//tutorial, but changed
package entities;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import gamestates.Playing;
import levels.Level;
import utilz.LoadSave;
import static utilz.Constants.EnemyConstants.*;

public class EnemyManager {

	private final Playing playing;
	private BufferedImage[][] crabbyArr;
	private BufferedImage[][] skeletonArr;
	private List<Crabby> crabbies = new ArrayList<>();
	private ArrayList<Skeleton> skeletons = new ArrayList<>();

	/**
	 * Constructs an EnemyManager object.
	 *
	 * @param playing The Playing game state.
	 */
	public EnemyManager(Playing playing) {
		this.playing = playing;
		loadEnemyImgs();
	}

	/**
	 * Loads the enemies from the specified level.
	 *
	 * @param level The level containing the enemies.
	 */
	public void loadEnemies(Level level) {
		crabbies = level.getCrabs();
		skeletons = level.getSkeletons();
	}

	/**
	 * Updates the enemies in the game.
	 *
	 * @param lvlData The level data representing the game environment.
	 * @param player  The player entity in the game.
	 */
	public void update(int[][] lvlData, Player player) {
		boolean isAnyActive = false;
		for (Crabby c : crabbies)
			if (c.isActive()) {
				c.update(lvlData, player);
				isAnyActive = true;
			}
		for (Skeleton s : skeletons)
			if (s.isActive()) {
				s.update(lvlData, player);
				isAnyActive = true;
			}
		if (!isAnyActive)
			playing.setLevelCompleted(true);
	}

	/**
	 * Draws the enemies on the screen.
	 *
	 * @param g          The Graphics object for drawing.
	 * @param xLvlOffset The x offset of the level.
	 */
	public void draw(Graphics g, int xLvlOffset) {
		drawCrabs(g, xLvlOffset);
		drawSkeletons(g, xLvlOffset);
	}

	/**
	 * Draws the crabs on the screen.
	 *
	 * @param g          The Graphics object for drawing.
	 * @param xLvlOffset The x offset of the level.
	 */
	private void drawCrabs(Graphics g, int xLvlOffset) {
		for (Crabby c : crabbies)
			if (c.isActive()) {
				g.drawImage(crabbyArr[c.getState()][c.getAniIndex()],
						(int) c.getHitbox().x - xLvlOffset - CRABBY_DRAWOFFSET_X + c.flipX(), //-CRABBYDRAwo
						(int) c.getHitbox().y - CRABBY_DRAWOFFSET_Y,
						CRABBY_WIDTH * c.flipW(), CRABBY_HEIGHT, null);
				c.drawHealthbar(g, xLvlOffset);
			}
	}
	private void drawSkeletons(Graphics g, int xLvlOffset) {
		for (Skeleton c : skeletons)
			if (c.isActive()) {
				g.drawImage(skeletonArr[c.getState()][c.getAniIndex()],
						(int) c.getHitbox().x - xLvlOffset - SKELETON_DRAWOFFSET_X + c.flipX(),
						(int) c.getHitbox().y - SKELETON_DRAWOFFSET_Y,
						SKELETON_WIDTH * c.flipW(), SKELETON_HEIGHT, null);
				c.drawHealthbar(g, xLvlOffset);
			}
	}

	/**
	 * Checks if the enemy is hit by the player's attack.
	 *
	 * @param attackBox The attack box of the player.
	 */
	public void checkEnemyHit(Rectangle2D.Float attackBox) {
		for (Crabby c : crabbies)
			if (c.isActive())
				if (attackBox.intersects(c.getHitbox())) {
					c.hurt(20);
					return;
				}
		for (Skeleton c : skeletons)
			if (c.isActive())
				if (attackBox.intersects(c.getHitbox())) {
					c.hurt(20);
					return;
				}
	}

	/**
	 * Loads the images for the enemies.
	 */
	private void loadEnemyImgs() {
		crabbyArr = new BufferedImage[5][9];
		BufferedImage crab = LoadSave.GetSpriteAtlas(LoadSave.CRABBY_SPRITE);
		for (int j = 0; j < crabbyArr.length; j++)
			for (int i = 0; i < crabbyArr[j].length; i++)
				crabbyArr[j][i] = crab.getSubimage(i * CRABBY_WIDTH_DEFAULT,
						j * CRABBY_HEIGHT_DEFAULT, CRABBY_WIDTH_DEFAULT, CRABBY_HEIGHT_DEFAULT);

		skeletonArr = new BufferedImage[5][8];
		BufferedImage skel = LoadSave.GetSpriteAtlas(LoadSave.SKELETON_SPRITE);
		for (int j = 0; j < skeletonArr.length; j++)
			for (int i = 0; i < skeletonArr[j].length; i++)
				skeletonArr[j][i] = skel.getSubimage(i * SKELETON_WIDTH_DEFAULT,
						j * SKELETON_HEIGHT_DEFAULT, SKELETON_WIDTH_DEFAULT, SKELETON_HEIGHT_DEFAULT);
	}

	/**
	 * Resets all the enemies to their initial state.
	 */
	public void resetAllEnemies() {
		for (Crabby c : crabbies)
			c.resetEnemy();
		for (Skeleton c : skeletons)
			c.resetEnemy();
	}
}
