//my
package inventory;

import utilz.LoadSave;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Coin {
    private int amount;

    public Coin(int coinAmount){
        this.amount = coinAmount;
    }
    public void drawCoin(Graphics g, int coinAmount, int x, int y, int scaledWidth, int scaledHeight) {
        BufferedImage coinImage = LoadSave.GetSpriteAtlas(LoadSave.COIN);
        coinImage = coinImage.getSubimage( 0, 0, 190, 170);
        String coinText = coinAmount + "";
        g.setColor(Color.white);
        Font font = new Font("Arial", Font.PLAIN, 16);
        g.setFont(font);

        BufferedImage scaledPotionImage = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = scaledPotionImage.createGraphics();
        g2d.drawImage(coinImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        g.drawImage(scaledPotionImage, x, y, null);
        g.drawString(coinText,x + 55, y + 30);
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
}
