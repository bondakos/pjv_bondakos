//my
package inventory;
import utilz.LoadSave;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Shield {
    private int amount;
    public Shield(int shieldAmount){
        this.amount = shieldAmount;
    }
    public void drawShield(Graphics g, int shieldAmount, int x, int y, int scaledWidth, int scaledHeight) {
        BufferedImage shieldImage = LoadSave.GetSpriteAtlas(LoadSave.SHIELD);
        String shieldText = shieldAmount + "";
        g.setColor(Color.white);
        Font font = new Font("Arial", Font.PLAIN, 16);
        g.setFont(font);

        BufferedImage scaledPotionImage = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = scaledPotionImage.createGraphics();
        g2d.drawImage(shieldImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        g.drawImage(scaledPotionImage, x, y, null);
        g.drawString(shieldText, x + 50, y + 30);
    }
    public int getAmount() {
        return amount;
    }
    public boolean getAddingShield() {
        return true;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
}
