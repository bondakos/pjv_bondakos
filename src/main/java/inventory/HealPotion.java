//my
package inventory;

import utilz.LoadSave;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The Potion class represents a health potion in the game's inventory.
 */
public class HealPotion {
    private int amount;

    /**
     * Constructs a new Potion object with the specified amount.
     *
     * @param poisonAmount the initial amount of the potion
     */
    public HealPotion(int poisonAmount){
        this.amount = poisonAmount;
    }

    /**
     * Draws the potion on the graphics context at the specified position with the current potion amount.
     *
     * @param g            the Graphics object to draw on
     * @param potionAmount the current amount of the potion
     * @param x            the x-coordinate of the position to draw the potion
     * @param y            the y-coordinate of the position to draw the potion
     */
    public void drawPotion(Graphics g, int potionAmount, int x, int y, int scaledWidth, int scaledHeight) {
        BufferedImage potionImage = LoadSave.GetSpriteAtlas(LoadSave.HEAL_POTION);
        String potionText = potionAmount + "";
        g.setColor(Color.white);
        Font font = new Font("Arial", Font.PLAIN, 16);
        g.setFont(font);

        BufferedImage scaledPotionImage = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = scaledPotionImage.createGraphics();
        g2d.drawImage(potionImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        g.drawImage(scaledPotionImage, x, y, null);
        g.drawString(potionText, x + 60, y + 40);
    }


    /**
     * Returns the amount of health the potion adds when consumed.
     *
     * @return the amount of health added by the potion
     */
    public int getAddingHealth() {
        return 20;
    }

    /**
     * Returns the current amount of the potion.
     *
     * @return the current amount of the potion
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Sets the amount of the potion.
     *
     * @param amount the new amount of the potion
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }
}
