import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ImageEditor extends JFrame {
    private BufferedImage image;
    private final JLabel imageLabel;
    private Color currentColor = Color.BLACK;
    private int zoomLevel = 100;
    private int previousX, previousY;
    private static final Logger LOGGER = Logger.getLogger(ImageEditor.class.getName());

    public ImageEditor() {
        setTitle("Image Editor");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        imageLabel = new JLabel();
        add(imageLabel, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();
        add(buttonPanel, BorderLayout.SOUTH);

        JButton openButton = new JButton("Open Image");
        openButton.addActionListener(e -> openImage());
        buttonPanel.add(openButton);

        JButton saveButton = new JButton("Save Image");
        saveButton.addActionListener(e -> saveImage());
        buttonPanel.add(saveButton);

        JButton exitButton = new JButton("Exit");
        exitButton.addActionListener(e -> System.exit(0));
        buttonPanel.add(exitButton);

        JPanel colorPanel = new JPanel();
        add(colorPanel, BorderLayout.NORTH);

        //list of colors
        Color customColor1 = new Color(11, 0, 154);
        Color customColor2 = new Color(11, 1, 60);
        Color customColor3 = new Color(11, 100, 6);

        Color[] customColors = {
                customColor1,
                customColor2,
                customColor3};
        String[] colorTexts = {
                "Crabby",
                "Skeleton",
                "Coin"};

        for (int i = 0; i < customColors.length; i++) {
            JButton colorButton = new JButton(colorTexts[i]);
            colorButton.setBackground(customColors[i]);
            colorButton.setForeground(Color.WHITE);
            colorButton.setOpaque(true);
            int finalI = i;
            colorButton.addActionListener(e -> setCurrentColor(customColors[finalI]));
            colorPanel.add(colorButton);
        }

        imageLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    drawPixel(e.getX(), e.getY());
                    System.out.println("colored");
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    previousX = e.getX();
                    previousY = e.getY();
                    imageLabel.requestFocusInWindow();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    imageLabel.setIgnoreRepaint(false);
                }
            }
        });

        imageLabel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    drawPixel(e.getX(), e.getY());
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    int dx = previousX - e.getX();
                    int dy = previousY - e.getY();
                    previousX = e.getX();
                    previousY = e.getY();
                    JViewport viewport = (JViewport) SwingUtilities.getAncestorOfClass(JViewport.class, imageLabel);
                    if (viewport != null) {
                        Point viewPos = viewport.getViewPosition();
                        viewPos.translate(dx, dy);
                        imageLabel.scrollRectToVisible(new Rectangle(viewPos, viewport.getSize()));
                    }
                }
            }
        });

        imageLabel.addMouseWheelListener(e -> {
            if (image != null) {
                int rotation = e.getWheelRotation();
                if (rotation < 0) {
                    zoomLevel += 10;
                } else {
                    zoomLevel -= 10;
                }
                if (zoomLevel < 10) {
                    zoomLevel = 10;
                }
                updateImage();
            }
        });

        pack();
        setVisible(true);
    }
    private void openImage() {
        JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showOpenDialog(this);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            try {
                image = ImageIO.read(selectedFile);
                ImageIcon icon = new ImageIcon(image);
                imageLabel.setIcon(icon);

                GridBagConstraints gbc = new GridBagConstraints();
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.anchor = GridBagConstraints.CENTER;

                JPanel imagePanel = new JPanel(new GridBagLayout());
                imagePanel.add(imageLabel, gbc);

                getContentPane().remove(imageLabel);
                getContentPane().add(imagePanel, BorderLayout.CENTER);
                revalidate();
                repaint();

                pack();
                setLocationRelativeTo(null);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "An error occurred while opening source file", e);
            }
        }
    }


    private void saveImage() {
        if (image != null) {
            JFileChooser fileChooser = new JFileChooser();
            int returnValue = fileChooser.showSaveDialog(this);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                try {
                    ImageIO.write(image, "png", selectedFile);
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "An error occurred while saving result file", e);
                }
            }
        }
    }

    private void setCurrentColor(Color color) {
        currentColor = color;
    }

    private void drawPixel(int x, int y) {
        if (image != null) {
            int scaledX = x * image.getWidth() / imageLabel.getWidth();
            int scaledY = y * image.getHeight() / imageLabel.getHeight();
            image.setRGB(scaledX, scaledY, currentColor.getRGB());
            System.out.println("colored in "+ currentColor);
            updateImage();
        }
    }

    private void updateImage() {
        if (image != null) {
            int newWidth = (image.getWidth() * zoomLevel) / 100;
            int newHeight = (image.getHeight() * zoomLevel) / 100;
            Image scaledImage = image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
            ImageIcon icon = new ImageIcon(scaledImage);
            imageLabel.setIcon(icon);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            ImageEditor imageEditor = new ImageEditor();
            imageEditor.setSize(600, 400);
            imageEditor.setLocationRelativeTo(null);
            imageEditor.setVisible(true);
        });
    }

}