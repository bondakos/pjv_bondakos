//tutorial

package main;

import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;

import javax.swing.JFrame;

/**
 * The GameWindow class represents the main game window that contains the game panel.
 */
public class GameWindow {

	/**
	 * Constructs a new GameWindow object with the specified GamePanel.
	 *
	 * @param gamePanel the GamePanel to be added to the window
	 */
	public GameWindow(GamePanel gamePanel) {

		JFrame jframe = new JFrame();

		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.add(gamePanel);

		jframe.setResizable(false);
		jframe.pack();
		jframe.setLocationRelativeTo(null);
		jframe.setVisible(true);
		jframe.addWindowFocusListener(new WindowFocusListener() {

			@Override
			public void windowLostFocus(WindowEvent e) {
				gamePanel.getGame().windowFocusLost();
			}

			@Override
			public void windowGainedFocus(WindowEvent e) {
				//auto generated method from lib. so i can't delete it
			}
		});

	}

}
