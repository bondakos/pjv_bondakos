//tutorial
package main;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;
import inputs.KeyboardInputs;
import inputs.MouseInputs;
import static main.Game.GAME_HEIGHT;
import static main.Game.GAME_WIDTH;

/**
 * The GamePanel class represents the panel where the game graphics are rendered and user inputs are captured.
 */
public class GamePanel extends JPanel {

	private MouseInputs mouseInputs;
	private final Game game;

	/**
	 * Constructs a new GamePanel object with the specified Game instance.
	 *
	 * @param game the Game instance
	 */
	public GamePanel(Game game) {
		mouseInputs = new MouseInputs(this);
		this.game = game;
		setPanelSize();
		addKeyListener(new KeyboardInputs(this));
		addMouseListener(mouseInputs);
		addMouseMotionListener(mouseInputs);
	}

	private void setPanelSize() {
		Dimension size = new Dimension(GAME_WIDTH, GAME_HEIGHT);
		setPreferredSize(size);
	}

	/**
	 * Updates the game state and components.
	 */
	public void updateGame() {

	}

	/**
	 * Renders the game graphics.
	 *
	 * @param g the Graphics object to draw on
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		game.render(g);
	}

	/**
	 * Returns the Game instance associated with the panel.
	 *
	 * @return the Game instance
	 */
	public Game getGame() {
		return game;
	}

}
