//tutorial, but changed
package main;

import java.awt.Graphics;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import gamestates.Gamestate;
import gamestates.Menu;
import gamestates.Playing;
import levels.LevelManager;


/**
 * The Game class represents the main game loop and manages the game's states and components.
 */
public class Game implements Runnable {
	private static final Logger LOGGER = Logger.getLogger(LevelManager.class.getName());

	private static final Logger logger = Logger.getLogger(Game.class.getName());
	private final GamePanel gamePanel;

	private Playing playing;
	private Menu menu;

	public final static int TILES_DEFAULT_SIZE = 32;
	public final static float SCALE = 1.7f;
	public final static int TILES_IN_WIDTH = 26;
	public final static int TILES_IN_HEIGHT = 14;
	public final static int TILES_SIZE = (int) (TILES_DEFAULT_SIZE * SCALE);
	public final static int GAME_WIDTH = TILES_SIZE * TILES_IN_WIDTH;
	public final static int GAME_HEIGHT = TILES_SIZE * TILES_IN_HEIGHT;

	/**
	 * Constructs a new Game object with the specified command-line arguments.
	 *
	 * @param args the command-line arguments
	 */
	public Game(String[] args) {
		initLogger(args);
		initClasses();
		gamePanel = new GamePanel(this);
		//if you try to delete it, the window would not appear
		GameWindow gameWindow = new GameWindow(gamePanel);
		gamePanel.setFocusable(true);
		gamePanel.requestFocus();

		startGameLoop();
	}

	private void initLogger(String[] args) {
		Logger rootLogger = Logger.getLogger("");
		rootLogger.setLevel(Level.INFO);
		ConsoleHandler consoleHandler = new ConsoleHandler();
		consoleHandler.setLevel(Level.INFO);
		rootLogger.addHandler(consoleHandler);

		for (String s : args) {
			if (s.equals("LOG_OFF")) {
				System.out.println("Logging off");
				rootLogger.setLevel(Level.OFF);
			} else {
				System.out.println("Logging on");
			}
		}
	}

	private void initClasses() {
		menu = new Menu(this);
		playing = new Playing(this);
	}

	private void startGameLoop() {
		Thread gameThread = new Thread(this);
		gameThread.start();
	}

	/**
	 * Updates the game state and components.
	 */
	public void update() {
        switch (Gamestate.state) {
            case MENU -> menu.update();
            case PLAYING -> playing.update();
            default -> {
                logger.severe("Invalid game state");
                System.exit(0);
            }
        }
	}

	/**
	 * Renders the game graphics.
	 *
	 * @param g the Graphics object to draw on
	 */
	public void render(Graphics g) {
        switch (Gamestate.state) {
            case MENU -> menu.draw(g);
            case PLAYING -> playing.draw(g);
            default -> {
            }
        }
	}

	@Override
	public void run() {
		int FPS_SET = 120;
		double timePerFrame = 1000000000.0 / FPS_SET;
		int UPS_SET = 200;
		double timePerUpdate = 1000000000.0 / UPS_SET;

		long previousTime = System.nanoTime();

		long lastCheck = System.currentTimeMillis();

		double deltaU = 0;
		double deltaF = 0;

		while (true) {
			long currentTime = System.nanoTime();

			deltaU += (currentTime - previousTime) / timePerUpdate;
			deltaF += (currentTime - previousTime) / timePerFrame;
			previousTime = currentTime;

			try {
				if (deltaU >= 1) {
					update();
					deltaU--;
				}

				if (deltaF >= 1) {
					gamePanel.repaint();
					deltaF--;
				}

				if (System.currentTimeMillis() - lastCheck >= 1000) {
					lastCheck = System.currentTimeMillis();
				}
			} catch (Exception e) {
				LOGGER.log(Level.SEVERE, "An exception occurred in the game loop", e);
				break;
			}
		}

	}

	/**
	 * Handles the event when the game window loses focus.
	 */
	public void windowFocusLost() {
		if (Gamestate.state == Gamestate.PLAYING)
			playing.getPlayer().resetDirBooleans();
	}

	/**
	 * Returns the Menu object.
	 *
	 * @return the Menu object
	 */
	public Menu getMenu() {
		return menu;
	}

	/**
	 * Returns the Playing object.
	 *
	 * @return the Playing object
	 */
	public Playing getPlaying() {
		return playing;
	}

	/**
	 * Returns the logger instance used for logging.
	 *
	 * @return the logger instance
	 */
	public Logger getLogger() {
		return logger;
	}
}
