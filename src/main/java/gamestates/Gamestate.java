package gamestates;

/**
 * The Gamestate enumeration represents the possible states of the game.
 * The game can be in one of the following states: PLAYING, MENU, OPTIONS, or QUIT.
 */
public enum Gamestate {

	PLAYING,

	MENU,

	OPTIONS,

	QUIT;

	public static Gamestate state = MENU;
}
