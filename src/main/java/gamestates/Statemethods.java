package gamestates;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Interface for game state methods.
 */
public interface Statemethods {
	/**
	 * Update method called to update the state.
	 */
    void update();

	/**
	 * Draw method called to render the state.
	 *
	 * @param g The Graphics object for drawing.
	 */
    void draw(Graphics g);

	/**
	 * Method called when a mouse click event occurs.
	 *
	 * @param e The MouseEvent object containing event details.
	 */
    void mouseClicked(MouseEvent e);

	/**
	 * Method called when a mouse press event occurs.
	 *
	 * @param e The MouseEvent object containing event details.
	 */
    void mousePressed(MouseEvent e);

	/**
	 * Method called when a mouse release event occurs.
	 *
	 * @param e The MouseEvent object containing event details.
	 */
    void mouseReleased(MouseEvent e);

	/**
	 * Method called when a mouse move event occurs.
	 *
	 * @param e The MouseEvent object containing event details.
	 */
    void mouseMoved(MouseEvent e);

	/**
	 * Method called when a key press event occurs.
	 *
	 * @param e The KeyEvent object containing event details.
	 */
    void keyPressed(KeyEvent e);

	/**
	 * Method called when a key release event occurs.
	 *
	 * @param e The KeyEvent object containing event details.
	 */
    void keyReleased(KeyEvent e);
}