//tutorial
package gamestates;

import java.awt.event.MouseEvent;

import main.Game;
import ui.MenuButton;

/**
 * The base class for game states.
 */
public class State {

	protected Game game;

	/**
	 * Constructs a new State object.
	 *
	 * @param game The game object.
	 */
	public State(Game game) {
		this.game = game;
	}

	/**
	 * Checks if a MouseEvent is within the bounds of a MenuButton.
	 *
	 * @param e  The MouseEvent.
	 * @param mb The MenuButton.
	 * @return true if the MouseEvent is within the bounds of the MenuButton, false otherwise.
	 */
	public boolean isIn(MouseEvent e, MenuButton mb) {
		return mb.getBounds().contains(e.getX(), e.getY());
	}

	/**
	 * Retrieves the Game object associated with this state.
	 *
	 * @return The Game object.
	 */
	public Game getGame() {
		return game;
	}
}