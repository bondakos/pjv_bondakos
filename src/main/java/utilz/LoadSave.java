//tutorial, but changed and optimalized

package utilz;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

public class LoadSave {
	private static final Logger LOGGER = Logger.getLogger(LoadSave.class.getName());
	public static final String PLAYER_ATLAS = "player_sprites.png";
	public static final String LEVEL_ATLAS = "outside_sprites.png";
	public static final String MENU_BUTTONS = "button_atlas.png";
	public static final String MENU_BACKGROUND = "menu_background.png";
	public static final String PAUSE_BACKGROUND = "pause_menu.png";
	public static final String SOUND_BUTTONS = "sound_button.png";
	public static final String URM_BUTTONS = "urm_buttons.png";
	public static final String VOLUME_BUTTONS = "volume_buttons.png";
	public static final String MENU_BACKGROUND_IMG = "background_menu.png";
	public static final String PLAYING_BG_IMG = "playing_bg_img.png";
	public static final String BIG_CLOUDS = "big_clouds.png";
	public static final String SMALL_CLOUDS = "small_clouds.png";
	public static final String CRABBY_SPRITE = "crabby_sprite.png";
	public static final String STATUS_BAR = "health_power_bar.png";
	public static final String COMPLETED_IMG = "completed_sprite.png";
	public static final String HEAL_POTION = "heal_potion.png";
	public static final String ENERGY_POTION = "energy_potion.png";
	public static final String SHIELD = "shield.png";
	public static final String SPIKE = "spike.png";
	public static final String CHAINSAW = "chainsaw.png";
	public static final String COIN = "coin.png";
	public static final String JUMPER = "jumper.png";
	public static final String SKELETON_SPRITE = "skeleton_sprites.png";

	/**
	 * Retrieves a sprite atlas image with the specified file name.
	 *
	 * @param fileName The name of the sprite atlas image file.
	 * @return The loaded sprite atlas image as a BufferedImage.
	 */
	public static BufferedImage GetSpriteAtlas(String fileName) {
		BufferedImage img = null;
		InputStream is = LoadSave.class.getResourceAsStream("/" + fileName);
		try {
			assert is != null;
			img = ImageIO.read(is);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "An error occurred while reading the image", e);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, "An error occurred while closing the input stream", e);
			}
		}
		return img;
	}

	/**
	 * Retrieves an array of all level images.
	 *
	 * @return An array of BufferedImage objects representing the level images.
	 */
	public static BufferedImage[] GetAllLevels() {
		URL url = LoadSave.class.getResource("/lvls");
		File file = null;

		try {
			file = new File(Objects.requireNonNull(url).toURI());
		} catch (URISyntaxException e) {
			LOGGER.log(Level.SEVERE, "An error occurred while getting resource URI", e);
		}

		File[] files = Objects.requireNonNull(file).listFiles();
		File[] filesSorted = new File[Objects.requireNonNull(files).length];

		for (int i = 0; i < filesSorted.length; i++) {
			for (File value : files) {
				if (value.getName().equals((i + 1) + ".png"))
					filesSorted[i] = value;
			}
		}

		BufferedImage[] imgs = new BufferedImage[filesSorted.length];

		for (int i = 0; i < imgs.length; i++) {
			try {
				imgs[i] = ImageIO.read(filesSorted[i]);
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "An error occurred while reading image", e);
			}
		}

		return imgs;
	}

}
