package utilz;

import static utilz.Constants.EnemyConstants.CRABBY;
import static utilz.Constants.EnemyConstants.SKELETON;
import static utilz.Constants.ObjectConstants.*;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import entities.Crabby;
import entities.Skeleton;
import main.Game;
import objects.Chainsaw;
import objects.Coin;
import objects.Jumper;
import objects.Spike;

public class HelpMethods {
	/**
	 * Checks if an entity can move to the specified position without colliding with any solid tiles.
	 *
	 * @param x        The x-coordinate of the position to check.
	 * @param y        The y-coordinate of the position to check.
	 * @param width    The width of the entity.
	 * @param height   The height of the entity.
	 * @param lvlData  The level data representing the solid tiles.
	 * @return         {@code true} if the entity can move to the specified position, {@code false} otherwise.
	 */
	public static boolean CanMoveHere(float x, float y, float width, float height, int[][] lvlData) {
		if (!IsSolid(x, y, lvlData))
			if (!IsSolid(x + width, y + height, lvlData))
				if (!IsSolid(x + width, y, lvlData))
                    return !IsSolid(x, y + height, lvlData);
		return false;
	}
	/**
	 * Checks if the specified position contains a solid tile.
	 *
	 * @param x        The x-coordinate of the position to check.
	 * @param y        The y-coordinate of the position to check.
	 * @param lvlData  The level data representing the solid tiles.
	 * @return         {@code true} if the position contains a solid tile, {@code false} otherwise.
	 */
	public static boolean IsSolid(float x, float y, int[][] lvlData) {
		int maxWidth = lvlData[0].length * Game.TILES_SIZE;
		if (x < 0 || x >= maxWidth)
			return true;
		if (y < 0 || y >= Game.GAME_HEIGHT)
			return true;
		float xIndex = x / Game.TILES_SIZE;
		float yIndex = y / Game.TILES_SIZE;

		return IsTileSolid((int) xIndex, (int) yIndex, lvlData);
	}
	/**
	 * Checks if the tile at the specified coordinates is solid.
	 *
	 * @param xTile    The x-coordinate of the tile.
	 * @param yTile    The y-coordinate of the tile.
	 * @param lvlData  The level data representing the solid tiles.
	 * @return         {@code true} if the tile is solid, {@code false} otherwise.
	 */
	public static boolean IsTileSolid(int xTile, int yTile, int[][] lvlData) {
		int value = lvlData[yTile][xTile];

        return value != 11;
    }
	/**
	 * Calculates the x-coordinate of the entity's position next to a wall.
	 *
	 * @param hitbox   The hitbox of the entity.
	 * @param xSpeed   The horizontal speed of the entity.
	 * @return         The x-coordinate of the entity's position next to a wall.
	 */
	public static float GetEntityXPosNextToWall(Rectangle2D.Float hitbox, float xSpeed) {
		int currentTile = (int) (hitbox.x / Game.TILES_SIZE);
		if (xSpeed > 0) {
			// Right
			int tileXPos = currentTile * Game.TILES_SIZE;
			int xOffset = (int) (Game.TILES_SIZE - hitbox.width);
			return tileXPos + xOffset - 1;
		} else
			// Left
			return currentTile * Game.TILES_SIZE;
	}
	/**
	 * Calculates the y-coordinate of the entity's position under a roof or above a floor.
	 *
	 * @param hitbox   The hitbox of the entity.
	 * @param airSpeed The vertical speed of the entity.
	 * @return         The y-coordinate of the entity's position under a roof or above a floor.
	 */
	public static float GetEntityYPosUnderRoofOrAboveFloor(Rectangle2D.Float hitbox, float airSpeed) {
		int currentTile = (int) (hitbox.y / Game.TILES_SIZE);
		if (airSpeed > 0) {
			// Falling - touching floor
			int tileYPos = currentTile * Game.TILES_SIZE;
			int yOffset = (int) (Game.TILES_SIZE - hitbox.height);
			return tileYPos + yOffset - 1;
		} else
			// Jumping
			return currentTile * Game.TILES_SIZE;

	}
	/**
	 * Checks if the entity is on the floor.
	 *
	 * @param hitbox   The hitbox of the entity.
	 * @param lvlData  The level data representing the solid tiles.
	 * @return         {@code true} if the entity is on the floor, {@code false} otherwise.
	 */
	public static boolean IsEntityOnFloor(Rectangle2D.Float hitbox, int[][] lvlData) {
		if (!IsSolid(hitbox.x, hitbox.y + hitbox.height + 1, lvlData))
            return IsSolid(hitbox.x + hitbox.width, hitbox.y + hitbox.height + 1, lvlData);
		return true;
	}
	/**
	 * Checks if there is a floor at the entity's next position.
	 *
	 * @param hitbox   The hitbox of the entity.
	 * @param xSpeed   The horizontal speed of the entity.
	 * @param lvlData  The level data representing the solid tiles.
	 * @return         {@code true} if there is a floor at the entity's next position, {@code false} otherwise.
	 */
	public static boolean IsFloor(Rectangle2D.Float hitbox, float xSpeed, int[][] lvlData) {
		if (xSpeed > 0)
			return IsSolid(hitbox.x + hitbox.width + xSpeed, hitbox.y + hitbox.height + 1, lvlData);
		else
			return IsSolid(hitbox.x + xSpeed, hitbox.y + hitbox.height + 1, lvlData);
	}
	/**
	 * Checks if all tiles within the specified range on a given row are walkable.
	 *
	 * @param xStart   The starting x-coordinate of the range.
	 * @param xEnd     The ending x-coordinate of the range.
	 * @param y        The y-coordinate of the row.
	 * @param lvlData  The level data representing the solid tiles.
	 * @return         {@code true} if all tiles in the range are walkable, {@code false} otherwise.
	 */
	public static boolean IsAllTilesWalkable(int xStart, int xEnd, int y, int[][] lvlData) {
		for (int i = 0; i < xEnd - xStart; i++) {
			if (IsTileSolid(xStart + i, y, lvlData))
				return false;
			if (!IsTileSolid(xStart + i, y + 1, lvlData))
				return false;
		}
		return true;
	}
	/**
	 * Checks if there is a clear line of sight between two hitboxes on a specific row.
	 *
	 * @param lvlData       The level data representing the solid tiles.
	 * @param firstHitbox   The first hitbox.
	 * @param secondHitbox  The second hitbox.
	 * @param yTile         The y-coordinate of the row to check.
	 * @return              {@code true} if there is a clear line of sight, {@code false} otherwise.
	 */
	public static boolean IsSightClear(int[][] lvlData, Rectangle2D.Float firstHitbox, Rectangle2D.Float secondHitbox, int yTile) {
		int firstXTile = (int) (firstHitbox.x / Game.TILES_SIZE);
		int secondXTile = (int) (secondHitbox.x / Game.TILES_SIZE);

		if (firstXTile > secondXTile)
			return IsAllTilesWalkable(secondXTile, firstXTile, yTile, lvlData);
		else
			return IsAllTilesWalkable(firstXTile, secondXTile, yTile, lvlData);
	}
	/**
	 * Converts an image into a level data array.
	 *
	 * @param img  The image representing the level.
	 * @return     The level data array.
	 */
	public static int[][] GetLevelData(BufferedImage img) {
		int[][] lvlData = new int[img.getHeight()][img.getWidth()];
		for (int j = 0; j < img.getHeight(); j++)
			for (int i = 0; i < img.getWidth(); i++) {
				Color color = new Color(img.getRGB(i, j));
				int value = color.getRed();
				if (value >= 48)
					value = 0;
				lvlData[j][i] = value;
			}
		return lvlData;
	}
	/**
	 * Extracts the locations of Crabby entities from an image.
	 *
	 * @param img  The image containing Crabby entities.
	 * @return     The list of Crabby entities.
	 */
	public static ArrayList<Crabby> GetCrabs(BufferedImage img) {
		ArrayList<Crabby> list = new ArrayList<>();
		for (int j = 0; j < img.getHeight(); j++)
			for (int i = 0; i < img.getWidth(); i++) {
				Color color = new Color(img.getRGB(i, j));
				int value = color.getGreen();
				if (value == CRABBY)
					list.add(new Crabby(i * Game.TILES_SIZE, j * Game.TILES_SIZE));
			}
		return list;
	}
	public static ArrayList<Skeleton> GetSkeletons(BufferedImage img) {
		ArrayList<Skeleton> list = new ArrayList<>();
		for (int j = 0; j < img.getHeight(); j++)
			for (int i = 0; i < img.getWidth(); i++) {
				Color color = new Color(img.getRGB(i, j));
				int value = color.getGreen();
				if (value == SKELETON)
					list.add(new Skeleton(i * Game.TILES_SIZE, j * Game.TILES_SIZE));
			}
		return list;
	}
	public static ArrayList<Spike> GetSpikes(BufferedImage img) {
		ArrayList<Spike> list = new ArrayList<>();

		for (int j = 0; j < img.getHeight(); j++)
			for (int i = 0; i < img.getWidth(); i++) {
				Color color = new Color(img.getRGB(i, j));
				int value = color.getBlue();
				if (value == SPIKE)
					list.add(new Spike(i * Game.TILES_SIZE, j * Game.TILES_SIZE, SPIKE));
			}
		return list;
	}
	public static ArrayList<Chainsaw> GetChainsaws(BufferedImage img) {
		ArrayList<Chainsaw> list = new ArrayList<>();
		for (int j = 0; j < img.getHeight(); j++)
			for (int i = 0; i < img.getWidth(); i++) {
				Color color = new Color(img.getRGB(i, j));
				int value = color.getBlue();
				if (value == CHAINSAW)
					list.add(new Chainsaw(i * Game.TILES_SIZE, j * Game.TILES_SIZE, CHAINSAW));
			}
		return list;
	}
	public static ArrayList<Coin> GetCoins(BufferedImage img) {
		ArrayList<Coin> list = new ArrayList<>();
		for (int j = 0; j < img.getHeight(); j++)
			for (int i = 0; i < img.getWidth(); i++) {
				Color color = new Color(img.getRGB(i, j));
				int value = color.getBlue();
				if (value == COIN)
					list.add(new Coin(i * Game.TILES_SIZE, j * Game.TILES_SIZE, COIN));

			}
		return list;
	}
	public static ArrayList<Jumper> GetJumpers(BufferedImage img) {
		ArrayList<Jumper> list = new ArrayList<>();
		for (int j = 0; j < img.getHeight(); j++)
			for (int i = 0; i < img.getWidth(); i++) {
				Color color = new Color(img.getRGB(i, j));
				int value = color.getBlue();
				if (value == JUMPER)
					list.add(new Jumper(i * Game.TILES_SIZE, j * Game.TILES_SIZE, JUMPER));
			}
		return list;
	}
	/**
	 * Extracts the player spawn point from an image.
	 *
	 * @param img  The image containing the player spawn point.
	 * @return     The player spawn point as a Point object.
	 */
	public static Point GetPlayerSpawn(BufferedImage img) {
		for (int j = 0; j < img.getHeight(); j++)
			for (int i = 0; i < img.getWidth(); i++) {
				Color color = new Color(img.getRGB(i, j));
				int value = color.getGreen();
				if (value == 100)
					return new Point(i * Game.TILES_SIZE, j * Game.TILES_SIZE);
			}
		return new Point(Game.TILES_SIZE, Game.TILES_SIZE);
	}

}