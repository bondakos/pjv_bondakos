//tutorial but some changes
package utilz;

import main.Game;

public class Constants {

	public static final float GRAVITY = 0.04f * Game.SCALE;
	public static final int ANI_SPEED = 25;
	public static class ObjectConstants{
		public static final int SPIKE = 4; //0bea4a
		public static final int CHAINSAW = 5; //0bea5a
		public static final int COIN = 6; //0bea6a
		public static final int JUMPER = 7; //0bea7a
		public static final int SPIKE_WIDTH_DEFAULT = 32;
		public static final int SPIKE_HEIGHT_DEFAULT = 32;
		public static final int SPIKE_WIDTH = (int) (Game.SCALE * SPIKE_WIDTH_DEFAULT);
		public static final int SPIKE_HEIGHT = (int) (Game.SCALE * SPIKE_HEIGHT_DEFAULT);
		public static int GetSpriteAmount(int object_type) {
			if (object_type == COIN) {
				return  6;
			}
				return  1;
		}
	}
	public static class EnemyConstants {
		public static final int CRABBY = 0;
		public static final int SKELETON = 1;
		public static final int IDLE = 0;
		public static final int RUNNING = 1;
		public static final int ATTACK = 2;
		public static final int HIT = 3;
		public static final int DEAD = 4;

		public static final int CRABBY_WIDTH_DEFAULT = 72;
		public static final int CRABBY_HEIGHT_DEFAULT = 32;
		public static final int SKELETON_WIDTH_DEFAULT = 32;
		public static final int SKELETON_HEIGHT_DEFAULT = 42;

		public static final int CRABBY_WIDTH = (int) (CRABBY_WIDTH_DEFAULT * Game.SCALE);
		public static final int CRABBY_HEIGHT = (int) (CRABBY_HEIGHT_DEFAULT * Game.SCALE);
		public static final int SKELETON_WIDTH = (int) (SKELETON_WIDTH_DEFAULT * Game.SCALE);
		public static final int SKELETON_HEIGHT = (int) (SKELETON_HEIGHT_DEFAULT * Game.SCALE);

		public static final int CRABBY_DRAWOFFSET_X = (int) (26 * Game.SCALE);
		public static final int CRABBY_DRAWOFFSET_Y = (int) (9 * Game.SCALE);
		public static final int SKELETON_DRAWOFFSET_X = (int) (6 * Game.SCALE);
		public static final int SKELETON_DRAWOFFSET_Y = (int) (10 * Game.SCALE);


		public static int GetSpriteAmount(int enemy_type, int enemy_state) {

            if (enemy_type == CRABBY) {
                switch (enemy_state) {
                    case IDLE -> {
                        return 9;
                    }
                    case RUNNING -> {
                        return 6;
                    }
                    case ATTACK -> {
                        return 7;
                    }
                    case HIT -> {
                        return 4;
                    }
                    case DEAD -> {
                        return 5;
                    }
                }
            }
			if (enemy_type == SKELETON) {
				switch (enemy_state) {
					case IDLE, RUNNING -> {
						return 5;
					}
					case ATTACK, DEAD -> {
						return 8;
					}
					case HIT -> {
						return 4;
					}
				}
			}
			return 0;
		}

		public static int GetMaxHealth(int enemy_type) {
            if (enemy_type == CRABBY) {
                return 40;
            }
			if(enemy_type == SKELETON){
				return 40;
			}
            return 1;
        }

		public static int GetEnemyDmg(int enemy_type) {
            return switch (enemy_type) {
                case CRABBY -> 15;
				case SKELETON -> 10;
                default -> 0;
            };

		}

	}

	public static class Environment {
		public static final int BIG_CLOUD_WIDTH_DEFAULT = 448;
		public static final int BIG_CLOUD_HEIGHT_DEFAULT = 101;
		public static final int SMALL_CLOUD_WIDTH_DEFAULT = 74;
		public static final int SMALL_CLOUD_HEIGHT_DEFAULT = 24;

		public static final int BIG_CLOUD_WIDTH = (int) (BIG_CLOUD_WIDTH_DEFAULT * Game.SCALE);
		public static final int BIG_CLOUD_HEIGHT = (int) (BIG_CLOUD_HEIGHT_DEFAULT * Game.SCALE);
		public static final int SMALL_CLOUD_WIDTH = (int) (SMALL_CLOUD_WIDTH_DEFAULT * Game.SCALE);
		public static final int SMALL_CLOUD_HEIGHT = (int) (SMALL_CLOUD_HEIGHT_DEFAULT * Game.SCALE);
	}

	public static class UI {
		public static class Buttons {
			public static final int B_WIDTH_DEFAULT = 140;
			public static final int B_HEIGHT_DEFAULT = 56;
			public static final int B_WIDTH = (int) (B_WIDTH_DEFAULT * Game.SCALE);
			public static final int B_HEIGHT = (int) (B_HEIGHT_DEFAULT * Game.SCALE);
		}

		public static class PauseButtons {
			public static final int SOUND_SIZE_DEFAULT = 42;
			public static final int SOUND_SIZE = (int) (SOUND_SIZE_DEFAULT * Game.SCALE);
		}

		public static class URMButtons {
			public static final int URM_DEFAULT_SIZE = 56;
			public static final int URM_SIZE = (int) (URM_DEFAULT_SIZE * Game.SCALE);

		}

		public static class VolumeButtons {
			public static final int VOLUME_DEFAULT_WIDTH = 28;
			public static final int VOLUME_DEFAULT_HEIGHT = 44;
			public static final int SLIDER_DEFAULT_WIDTH = 215;

			public static final int VOLUME_WIDTH = (int) (VOLUME_DEFAULT_WIDTH * Game.SCALE);
			public static final int VOLUME_HEIGHT = (int) (VOLUME_DEFAULT_HEIGHT * Game.SCALE);
			public static final int SLIDER_WIDTH = (int) (SLIDER_DEFAULT_WIDTH * Game.SCALE);
		}
	}

	public static class Directions {
		public static final int LEFT = 0;
		public static final int RIGHT = 2;
	}

	public static class PlayerConstants {
		public static final int IDLE = 0;
		public static final int RUNNING = 1;
		public static final int JUMP = 2;
		public static final int FALLING = 3;
		public static final int ATTACK = 4;
		public static final int HIT = 5;
		public static final int DEAD = 6;

		public static int GetSpriteAmount(int player_action) {
			return switch (player_action) {
				case DEAD -> 8;
				case RUNNING -> 6;
				case IDLE -> 5;
				case HIT -> 4;
				case JUMP, ATTACK -> 3;
				default -> 1;
			};
		}
	}

}