//tutorial, but a little bit optimalised
package inputs;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Objects;

import gamestates.Gamestate;
import main.GamePanel;

/**
 * The MouseInputs class implements the MouseListener and MouseMotionListener interfaces to handle mouse input events.
 * It directs the input events to the appropriate game state based on the current state of the game.
 */
public class MouseInputs implements MouseListener, MouseMotionListener {

	private final GamePanel gamePanel;

	/**
	 * Constructs a new MouseInputs object with the specified GamePanel.
	 *
	 * @param gamePanel the GamePanel to associate with the MouseInputs
	 */
	public MouseInputs(GamePanel gamePanel) {
		this.gamePanel = gamePanel;
	}

	/**
	 * Invoked when the mouse is dragged. Delegates the mouse event to the appropriate game state based on the current state of the game.
	 *
	 * @param e the MouseEvent that occurred
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		if (Gamestate.state == Gamestate.PLAYING) {
			gamePanel.getGame().getPlaying().mouseDragged(e);
		}
	}

	/**
	 * Invoked when the mouse is moved. Delegates the mouse event to the appropriate game state based on the current state of the game.
	 *
	 * @param e the MouseEvent that occurred
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
        switch (Gamestate.state) {
            case MENU -> gamePanel.getGame().getMenu().mouseMoved(e);
            case PLAYING -> gamePanel.getGame().getPlaying().mouseMoved(e);
            default -> {
            }
        }
	}

	/**
	 * Invoked when the mouse is clicked. Delegates the mouse event to the appropriate game state based on the current state of the game.
	 *
	 * @param e the MouseEvent that occurred
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
        if (Objects.requireNonNull(Gamestate.state) == Gamestate.PLAYING) {
            gamePanel.getGame().getPlaying().mouseClicked(e);
        }
	}

	/**
	 * Invoked when a mouse button is pressed. Delegates the mouse event to the appropriate game state based on the current state of the game.
	 *
	 * @param e the MouseEvent that occurred
	 */
	@Override
	public void mousePressed(MouseEvent e) {
        switch (Gamestate.state) {
            case MENU -> gamePanel.getGame().getMenu().mousePressed(e);
            case PLAYING -> gamePanel.getGame().getPlaying().mousePressed(e);
            default -> {
            }
        }
	}

	/**
	 * Invoked when a mouse button is released. Delegates the mouse event to the appropriate game state based on the current state of the game.
	 *
	 * @param e the MouseEvent that occurred
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
        switch (Gamestate.state) {
            case MENU -> gamePanel.getGame().getMenu().mouseReleased(e);
            case PLAYING -> gamePanel.getGame().getPlaying().mouseReleased(e);
            default -> {
            }
        }
	}

	/**
	 * Invoked when the mouse enters a component. This method is not used in this implementation.
	 *
	 * @param e the MouseEvent that occurred
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
		// Not used in this implementation
	}

	/**
	 * Invoked when the mouse exits a component. This method is not used in this implementation.
	 *
	 * @param e the MouseEvent that occurred
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		// Not used in this implementation
	}
}
