//tutorial, but changed and optimalized
package inputs;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import gamestates.Gamestate;
import main.GamePanel;

/**
 * The KeyboardInputs class implements the KeyListener interface to handle keyboard input events.
 * It directs the input events to the appropriate game state based on the current state of the game.
 */
public class KeyboardInputs implements KeyListener {

	private final GamePanel gamePanel;

	/**
	 * Constructs a new KeyboardInputs object with the specified GamePanel.
	 *
	 * @param gamePanel the GamePanel to associate with the KeyboardInputs
	 */
	public KeyboardInputs(GamePanel gamePanel) {
		this.gamePanel = gamePanel;
	}

	/**
	 * Invoked when a key is typed. This method is not used in this implementation.
	 *
	 * @param e the KeyEvent that occurred
	 */
	@Override
	public void keyTyped(KeyEvent e) {
		// Not used in this implementation
	}

	/**
	 * Invoked when a key is released. Delegates the key event to the appropriate game state based on the current state of the game.
	 *
	 * @param e the KeyEvent that occurred
	 */
	@Override
	public void keyReleased(KeyEvent e) {
        switch (Gamestate.state) {
            case MENU -> gamePanel.getGame().getMenu().keyReleased(e);
            case PLAYING -> gamePanel.getGame().getPlaying().keyReleased(e);
            default -> {
            }
        }
	}

	/**
	 * Invoked when a key is pressed. Delegates the key event to the appropriate game state based on the current state of the game.
	 *
	 * @param e the KeyEvent that occurred
	 */
	@Override
	public void keyPressed(KeyEvent e) {
        switch (Gamestate.state) {
            case MENU -> gamePanel.getGame().getMenu().keyPressed(e);
            case PLAYING -> gamePanel.getGame().getPlaying().keyPressed(e);
            default -> {
            }
        }
	}
}
