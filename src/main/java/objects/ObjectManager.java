//tutorial 20%
package objects;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import entities.Player;
import gamestates.Playing;
import levels.Level;
import utilz.LoadSave;

import static utilz.Constants.ObjectConstants.*;

public class ObjectManager {

    private final Playing playing;
    private BufferedImage spikeImg;
    private BufferedImage chainsawImg;
    private BufferedImage[] coinImgs;
    private BufferedImage jumperImg;
    private ArrayList<Spike> spikes;
    private ArrayList<Chainsaw> chainsaws;
    private ArrayList<Coin> coins;
    private ArrayList<Jumper> jumpers;

    public ObjectManager(Playing playing) {
        this.playing = playing;
        loadImgs();
    }

    public void update(int[][] lvlData) {
        for (Chainsaw c : chainsaws)
            if (c.isActive()) {
                c.update(lvlData);
            }
        updateCoins();
    }
    private void updateCoins() {
        for (Coin c : coins) {
            if(c.isActive()){
                  c.setAnimation(true);
                  c.update();
        }
        }
    }
    public void checkSpikesTouched(Player p) {
        for (Spike s : spikes)
            if (s.getHitbox().intersects(p.getHitbox()))
                p.kill();
    }

    public void checkChainsawTouched(Player p) {
        for (Chainsaw c : chainsaws)
            if (c.isActive()) {
                if (c.getHitbox().intersects(p.getHitbox()))
                    p.changeHealth((float) -0.1);
            }
    }
    public void checkCoinTouched(Player p) {
        for (Coin c : coins)
            if (c.isActive()) {
                if (c.getHitbox().intersects(p.getHitbox())) {
                    p.addCoin();
                    c.setActive(false);
                    c.setAnimation(false);
                }
            }
    }
    public void checkJumperTouchedPlayer(Player p) {
        for (Jumper t : jumpers)
            if (t.isActive()) {
                if (t.getHitbox().intersects(p.getHitbox())) {
                    p.jumper((float) 1.5);
                }
            }
    }

    public void loadObjects(Level newLevel) {
        spikes = newLevel.getSpikes();
        chainsaws = newLevel.getChainsaws();
        coins = newLevel.getCoins();
        jumpers = newLevel.getJumpers();
    }

    public void loadImgs() {
        spikeImg = LoadSave.GetSpriteAtlas(LoadSave.SPIKE);
        chainsawImg = LoadSave.GetSpriteAtlas(LoadSave.CHAINSAW);


        coinImgs = new BufferedImage[6];
        BufferedImage coinSprite = LoadSave.GetSpriteAtlas(LoadSave.COIN);
        for (int i = 0; i < coinImgs.length; i++) {
            coinImgs[i] = coinSprite.getSubimage(190 * i, 0, 190, 170);
        }
        jumperImg = LoadSave.GetSpriteAtlas(LoadSave.JUMPER);
    }

    public void draw(Graphics g, int xLvlOffset) {
        drawObjects(g, xLvlOffset);
    }

    private void drawObjects(Graphics g, int xLvlOffset) {
        for (Spike s : spikes)
            g.drawImage(spikeImg, (int) (s.getHitbox().x - xLvlOffset),
                    (int) (s.getHitbox().y - s.getyDrawOffset()), SPIKE_WIDTH, SPIKE_HEIGHT, null);

        for (Chainsaw c : chainsaws)
            if (c.isActive()) {
                g.drawImage(chainsawImg, (int) (c.getHitbox().x - xLvlOffset),
                        (int) (c.getHitbox().y - c.getyDrawOffset()), SPIKE_WIDTH, SPIKE_HEIGHT, null);
            }
        for (Coin c : coins)
            if (c.isActive() && c.isAnimation())
                g.drawImage(coinImgs[c.getAniIndex()], (int) (c.getHitbox().x - xLvlOffset),
                        (int) (c.getHitbox().y - c.getyDrawOffset()), SPIKE_WIDTH, SPIKE_HEIGHT, null);


        for (Jumper c : jumpers)
            if (c.isActive()) {
                g.drawImage(jumperImg, (int) (c.getHitbox().x - xLvlOffset),
                        (int) (c.getHitbox().y), SPIKE_WIDTH, SPIKE_HEIGHT/2, null);
            }
    }
    public void resetAllObjects() {
        loadObjects(playing.getLevelManager().getCurrentLevel());
    }
}