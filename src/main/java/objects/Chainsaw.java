//my
package objects;

import main.Game;
import utilz.HelpMethods;

public class Chainsaw extends GameObject {

    private boolean moveRight;

    public Chainsaw(int x, int y, int objType) {
        super(x, y, objType);
        initHitbox(32, 14);
        xDrawOffset = 0;
        yDrawOffset = (int) (Game.SCALE * 16);
        hitbox.y += yDrawOffset;
        moveRight = false;
    }

    public void update(int[][] lvlData) {
        move(lvlData);
    }
    private void move(int[][] lvlData) {
        float speed = 0.7f;
        if (moveRight) {
            if (HelpMethods.CanMoveHere(hitbox.x + speed, hitbox.y, hitbox.width, hitbox.height, lvlData)){
                hitbox.x += speed * Game.SCALE;
            } else {
                moveRight = false;
            }
        } else {
            if (HelpMethods.CanMoveHere(hitbox.x - speed, hitbox.y, hitbox.width, hitbox.height, lvlData)) {
                hitbox.x -= speed * Game.SCALE;
            } else {
                moveRight = true;
            }
        }
    }

}
