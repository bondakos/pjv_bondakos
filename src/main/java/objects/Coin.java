//my
package objects;

import main.Game;

public class Coin extends GameObject{

    public Coin(int x, int y, int objType) {
        super(x, y, objType);
        initHitbox(32, 14);
        xDrawOffset = 0;
        yDrawOffset = (int) (Game.SCALE * 16);
        hitbox.y += yDrawOffset;
    }
    public void update() {
        if (animation)
            updateAnimationTick();
    }
}
