//my
package objects;

import main.Game;

public class Jumper extends GameObject{
    public Jumper(int x, int y, int objType) {
        super(x, y, objType);
        initHitbox(32, 14);
        xDrawOffset = 0;
        yDrawOffset = (int) (Game.SCALE * 16);
        hitbox.y += yDrawOffset;
    }
}
